<jsp:include page="/includes/header.jsp"></jsp:include>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6">
 <table class="w3-table">
  <tr>
    <th>Photo</th>
    <th>Name</th>
    <th>Quantity</th>
    <th>Price</th>
  </tr>
  <tbody>
  <c:forEach items="${orderlist}" var="flowers">
  <tr>
    <td>${flowers.orderNumber}</td>
    <td>${flowers.flowerName }</td>
    <td>${flowers.itemquantity }</td>
    <td>${flowers.subprice }</td>
  </tr>
  </c:forEach>
  </tbody>
</table>
</div>
</div>

<jsp:include page="/includes/footer.jsp"></jsp:include>
</body>
</html>