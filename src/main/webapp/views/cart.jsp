<jsp:include page="/includes/header.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 <meta charset="UTF-8">
 <meta name="description" content="Free Web tutorials">
 <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <div class="row">
<div class="col-sm-3"></div>
<div class="col-sm-6">
<a href='<spring:url value="/" />'>Continue buying</a>
<table class="w3-table">
  <tr>
    <th>Name</th>
    <th>Quantity</th>
    <th>Price</th>
    <th>Sub-total</th>
  </tr>
  
  <c:forEach items="${cart}" var="cart" >
  <tr >
    <td>${cart.flower.name }</td>
    <td>${cart.itemquantity}</td>
    <td>${cart.flower.price }</td>
    <td>${cart.flower.price*cart.itemquantity }</td>
  </tr>
  </c:forEach>
</table>
<a href='<spring:url value="/flower/customer/order"/>' class="btn btn-sucess">Buy</a>

</div>
<div class="col-sm-3"></div>
</div>
<footer style="border-bottom:0;">
		<jsp:include page="/includes/footer.jsp"></jsp:include>
</footer>
</body>
</html>

