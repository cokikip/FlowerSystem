package com.coki.mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.coki.mvc.data.UserRepository;
import com.coki.mvc.data.Users;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserRepository repo;
	@RequestMapping(value="/register",method=RequestMethod.GET)
	public String registerUser() {
		return "register";
		
	}
	@RequestMapping(value="/register",method=RequestMethod.POST)
	public String saveUser(@ModelAttribute Users user) {
		
		
		if (user.getRole().equals("customer")) {
			user.setRole("ROLE_CUSTOMER");
			
		} else{
			user.setRole("ROLE_FARMER");

		}
		repo.save(user);
		Authentication auth= new UsernamePasswordAuthenticationToken(user, user.getPassword(),user.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
		return "register";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login() {
		return "login";
		
	}

}
