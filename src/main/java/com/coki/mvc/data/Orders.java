package com.coki.mvc.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Orders {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="orderNumber")
	private int orderNumber;
	private String flowerName;
	private int itemquantity;
	private double subprice;
	private String customerName;
	
	
	public double getSubprice() {
		return subprice;
	}
	public void setSubprice(double subprice) {
		this.subprice = subprice;
	}
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getFlowerName() {
		return flowerName;
	}
	public void setFlowerName(String flowerName) {
		this.flowerName = flowerName;
	}
	
	
	public int getItemquantity() {
		return itemquantity;
	}
	public void setItemquantity(int itemquantity) {
		this.itemquantity = itemquantity;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	@Override
	public String toString() {
		return "Orders [orderNumber=" + orderNumber + ", flowerName=" + flowerName + ", itemquantity=" + itemquantity
				+ ", subprice=" + subprice + ", customerName=" + customerName + "]";
	}

}
