package com.coki.mvc.controllers;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.coki.mvc.data.Flower;
import com.coki.mvc.data.FlowerRepository;
import com.coki.mvc.data.Item;
import com.coki.mvc.data.Orders;
import com.coki.mvc.data.OrdersRepository;

@Controller
@RequestMapping("/flower")
@SessionAttributes("cart")
public class FlowerController {
	
	@Autowired
	FlowerRepository repo;
	@Autowired
	OrdersRepository repo1;
	
	@ModelAttribute(value="flower")
	public Flower getFlower() {
		return new Flower();			
	}
	
	@RequestMapping(value = "/farmer/add", method=RequestMethod.GET)
	public String addFlower() {
		return "addflower";
	}

	@RequestMapping(value = "/farmer/add", method=RequestMethod.POST)
	public String saveFlower(@ModelAttribute Flower flower ) throws Exception {
		String username=SecurityContextHolder.getContext().getAuthentication().getName();
		MultipartFile file=flower.getFile();
	    byte[] ibyte=file.getBytes();	
	    Blob blob=new SerialBlob(ibyte);
	    flower.setImage(blob);
	    System.out.println(flower);
		System.out.println("Data binding");
		flower.setFarmersName(username);
		repo.save(flower);		
		return "addflower";
	}
	
	
	@RequestMapping(value="/farmer/products")
	public String products(Model model,Pageable pageable) throws Exception {
		String farmersName=SecurityContextHolder.getContext().getAuthentication().getName();
		List<Flower> flowers=repo.findByFarmersName(farmersName);
		for (Flower flower : flowers) {
			Blob image=flower.getImage();
			int bloblength=(int) image.length();
			byte[] b=image.getBytes(1, bloblength);
			String baseImage =Base64.getEncoder().encodeToString(b);
			flower.setBase64image(baseImage);	
			
		}
		model.addAttribute("flowerlist", flowers);
		return "product-list";	
	}
	
	@RequestMapping(value="/farmer/delete")
	public String remove(@RequestParam int id) {
		repo.delete(id);
		return "redirect:/flower/farmer/products";
	}
	@RequestMapping(value="/farmer/edit")
	public String edit(@RequestParam int id,@ModelAttribute Flower flower) {
		ModelAndView nav=new ModelAndView("edit-flower");
		flower=repo.findOne(id);
		nav.addObject("flower", flower);
		return "redirect:/flower/farmer/products";
	} 
	
	@RequestMapping(value="/customer/find")
	public String findFlowers(Model model) throws Exception {
		List<Flower> flowers=repo.findAll();
		for (Flower flower : flowers) {
			Blob image=flower.getImage();
			int bloblength=(int) image.length();
			
			byte[] b=image.getBytes(1, bloblength);
			
			String baseImage =Base64.getEncoder().encodeToString(b);
			flower.setBase64image(baseImage);
			
		}
		model.addAttribute("flowerlist", flowers);
		return "flowers";	
	}
	
	@RequestMapping("/customer/{flowerId}")
	public String viewFlower(@PathVariable("flowerId") int flowerId, Model model) throws Exception {
		Flower flower=repo.findOne(flowerId);
		Blob image=flower.getImage();
		int bloblength=(int) image.length();

		byte[] b=image.getBytes(1, bloblength);
		
		String baseImage =Base64.getEncoder().encodeToString(b);
		flower.setBase64image(baseImage);
		model.addAttribute("flower", flower);
		return "flower";
	}
	
	@RequestMapping(value="/customer/buy/{id}", method=RequestMethod.POST)
	public String buy(@PathVariable int id,Model model,@ModelAttribute Item item,HttpServletRequest request) {
		HttpSession session = request.getSession();
		int itemquantity=item.getItemquantity();
		System.out.println(itemquantity);
		if(session.getAttribute("cart") == null) {
			List<Item> cart = new ArrayList<Item>();
			cart.add(new Item(repo.findOne(id), itemquantity));
	
			model.addAttribute("cart", cart);
		}else {
			@SuppressWarnings("unchecked")
			List<Item> cart =(List<Item>)session.getAttribute("cart");
			int index=isExisting(id, cart);
			if (index==-1) {
				cart.add(new Item(repo.findOne(id), itemquantity));
			} else {
				cart.get(index).setItemquantity(itemquantity);

			}
					
			System.out.println(cart);
			model.addAttribute("cart", cart);
			
		}

		return "cart";
	}

	private int isExisting(int id,List<Item> cart) {
		for (int i = 0; i < cart.size()	; i++) {
			if (cart.get(i).getFlower().getFlowerId()==id) {
				return i;
			}

		}
		return -1;
	}
	@RequestMapping(value="/customer/cart")
	public  String cart() {
		
		return "cart";
	}
     @RequestMapping(value="/customer/order")
	public String orders(HttpSession session) {
		String customerName=SecurityContextHolder.getContext().getAuthentication().getName();
		@SuppressWarnings("unchecked")
		List<Item> cart=(List<Item>)session.getAttribute("cart");
		for(Item order:cart) {
			System.out.println(order);
			int itemQuantity=order.getItemquantity();
			Orders od=new Orders();
			od.setFlowerName(order.getFlower().getName());
			od.setCustomerName(customerName);
			od.setItemquantity(itemQuantity);
			od.setSubprice(order.getFlower().getPrice()*itemQuantity);
			repo1.save(od);
			int id=order.getFlower().getFlowerId();
			Flower flower=repo.findOne(id);
			int newQuantity=(flower.getQuantity())-itemQuantity;
			flower.setQuantity(newQuantity);
			repo.save(flower);
		}
		cart.clear();
		return "redirect:/";
	}
     @RequestMapping(value="/customer/yourorders")
     public String yourorders(Model model) {
    	 List<Orders> orderlist=repo1.findAll();
         model.addAttribute("orderlist", orderlist);
	  return "your-orders";
    }

}